###
#   Testumgebung Kubernetes 

module "master" {
  source      = "./terraform-lerncloud-module"
  module     = "master-${var.host_no}-${terraform.workspace}"
  userdata   = "cloud-init-microk8smaster.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000, 32280 ]
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"  
 
}

module "worker-01" {
  source      = "./terraform-lerncloud-module"
  module     = "worker-${var.host_no + 1}-${terraform.workspace}"
  userdata   = "cloud-init-microk8sworker.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000, 32280 ]
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"  
}

/*
module "worker-02" {
  source      = "./terraform-lerncloud-module"
  module     = "worker-${var.host_no + 2}-${terraform.workspace}"
  userdata   = "cloud-init-microk8sworker.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000, 32280 ]
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"  
}
*/

terraform {
  backend "http" {
  }
}
